
; Drupal version
core = 7.x
api = 2

; Contrib modules
projects[debut_contact][subdir] = contrib
projects[debut_contact][version] = 1.0-beta1
projects[debut][subdir] = contrib
projects[ctools][subdir] = contrib
projects[features][subdir] = contrib
projects[node_export][subdir] = contrib
projects[strongarm][subdir] = contrib
projects[webform][subdir] = contrib
